<?php

require_once "./../vendor/autoload.php";
require_once __DIR__ . './../module/Application/src/Entity/Comment.php';

use Doctrine\ORM\Tools\Setup;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\Mapping\Driver\AnnotationDriver;
use Doctrine\Common\Annotations\AnnotationReader;
use Doctrine\Common\Annotations\AnnotationRegistry;

$paths = [
    './module/Application/src/Entity',
];
$isDevMode = true;

$dbParams = [
    'dbname' => 'chat_znd',
    'user' => 'alooner',
    'password' => 'alooner',
    'driver' => 'pdo_mysql',
];

//$config = Setup::createAnnotationMetadataConfiguration($paths, $isDevMode);

$config = Setup::createConfiguration($isDevMode);
$driver = new AnnotationDriver(new AnnotationReader(), $paths);

// registering noop annotation autoloader - allow all annotations by default
AnnotationRegistry::registerLoader('class_exists');
$config->setMetadataDriverImpl($driver);

