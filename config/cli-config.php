<?php

use Doctrine\ORM\Tools\Console\ConsoleRunner;
use Doctrine\ORM\EntityManager;

require_once 'bootstrap.php';

try {

    $entityManager = EntityManager::create($dbParams, $config);

} catch (\Doctrine\ORM\ORMException $exception) {

    dump('==================================================================');
    dump($exception);
    dump('==================================================================');
    $entityManager = null;

}

return ConsoleRunner::createHelperSet($entityManager);