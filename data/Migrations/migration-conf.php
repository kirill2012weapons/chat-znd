<?php

return [
    'name' => 'Migrations For comments',
    'migrations_namespace' => 'Application\Migrations\Comments',
    'table_name' => 'migrations_comments',
    'column_name' => 'version',
    'column_length' => 14,
    'executed_at_column_name' => 'executed_at',
    'migrations_directory' => './data/Migrations/Packages',
    'all_or_nothing' => true,
];
